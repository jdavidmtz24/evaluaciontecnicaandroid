package com.juandavid.demogenteramovies.data.model

import com.google.gson.annotations.SerializedName

data class PlayListMovieResponse(@SerializedName("id") val id:Long,
                                 @SerializedName("media_type") val results:ArrayList<Video>)
