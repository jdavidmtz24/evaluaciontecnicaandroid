package com.juandavid.demogenteramovies.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.juandavid.demogenteramovies.data.database.dao.MovieDao
import com.juandavid.demogenteramovies.data.database.entities.GenreEntity
import com.juandavid.demogenteramovies.data.database.entities.MoviesEntity
import com.juandavid.demogenteramovies.data.database.entities.PlayListMovieEntity

@Database(entities = [MoviesEntity::class, GenreEntity::class, PlayListMovieEntity::class], version = 1)
abstract class MovieDatabase :RoomDatabase() {

    abstract fun getMovieDao():MovieDao
}