package com.juandavid.demogenteramovies.data.network

import com.juandavid.demogenteramovies.BuildConfig
import com.juandavid.demogenteramovies.data.model.GenerMovieResponse
import com.juandavid.demogenteramovies.data.model.ListMoviesResponse
import com.juandavid.demogenteramovies.data.model.PlayListMovieResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieService @Inject constructor(private val api:MoviesApiClient) {


    suspend fun getMovies( listId:Int ):ListMoviesResponse?{
        return withContext(Dispatchers.IO){
            val response=api.getAllMovies(if (listId in 1..42) listId else 1, BuildConfig.API_KEY)//if (listId in 1..42) listId else 1
            response.body()
        }
    }

    suspend fun getGenres():GenerMovieResponse?{
        return withContext(Dispatchers.IO){
            val response = api.getAllGenres(BuildConfig.API_KEY)
            response.body()
        }
    }

    suspend fun getPlayListMovie(idMovie:Long):PlayListMovieResponse?{
        return withContext(Dispatchers.IO){
            val response = api.getAllVideoMovie(idMovie,BuildConfig.API_KEY)
            response.body()
        }
    }

}