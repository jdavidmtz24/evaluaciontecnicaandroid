package com.juandavid.demogenteramovies.data.database.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.juandavid.demogenteramovies.data.model.Video
import com.juandavid.demogenteramovies.domain.model.PlayListMovie
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "play_list_movie_table")
data class PlayListMovieEntity(@ColumnInfo("iso_639_1") val iso_639_1:String,
                               @ColumnInfo("iso_3166_1") val iso_3166_1:String,
                               @ColumnInfo("name") val name:String,
                               @ColumnInfo("key") val key:String,
                               @ColumnInfo("site") val site:String,
                               @ColumnInfo("size") val size:Long,
                               @ColumnInfo("type") val type:String,
                               @ColumnInfo("official") val official:String,
                               @ColumnInfo("published_at") val published_at:String,
                               @PrimaryKey @ColumnInfo("id") val id:String,
                               @ColumnInfo("idMovie") var idMovie:Long)

fun Video.toDatabase() = PlayListMovieEntity(iso_639_1 =iso_639_1, iso_3166_1 = iso_3166_1, name = name,
    key = key,site = site, size = size, type = type, official = official, published_at =published_at,
    id=id, idMovie = idMovie)

fun PlayListMovie.toDatabase() = PlayListMovieEntity(iso_639_1 =iso_639_1, iso_3166_1 = iso_3166_1, name = name,
    key = key,site = site, size = size, type = type, official = official, published_at =published_at,
    id=id, idMovie = idMovie)
