package com.juandavid.demogenteramovies.data

import com.juandavid.demogenteramovies.data.database.dao.MovieDao
import com.juandavid.demogenteramovies.data.database.entities.GenreEntity
import com.juandavid.demogenteramovies.data.database.entities.MoviesEntity
import com.juandavid.demogenteramovies.data.database.entities.PlayListMovieEntity
import com.juandavid.demogenteramovies.data.model.ListMoviesResponse
import com.juandavid.demogenteramovies.data.network.MovieService
import com.juandavid.demogenteramovies.domain.model.Genre
import com.juandavid.demogenteramovies.domain.model.Movie
import com.juandavid.demogenteramovies.domain.model.PlayListMovie
import com.juandavid.demogenteramovies.domain.model.toDomain
import javax.inject.Inject

class MoviesRepository @Inject constructor(private val api : MovieService,
                                           private val movieDao:MovieDao){


    suspend fun getAllMovieFromApi(listId:Int):List<Movie>?{
        val response:ListMoviesResponse? = api.getMovies(listId)

        return response?.results?.map {
            it.page_movie= listId
            it.toDomain()
        }

    }

    suspend fun getAllMovieFromDatabase(listId:Int):List<Movie>?{
        val response= movieDao.getMoviePage(listId)
        return response.map { it.toDomain() }
    }
    suspend fun getMovieFromApi(listId:Int):List<Movie>?{
        val response= api.getMovies(listId)

        return response?.results?.map { it.toDomain()}
    }

    suspend fun getMovieFromDatabase(listId:Int):Movie?{
        val response= movieDao.getMovieId(listId)
        return response.toDomain()
    }


    suspend fun getAllGenresFromApi(): List<Genre>?{
        val response= api.getGenres()
        return response?.genres?.map {
            it.toDomain()
        }

    }

    suspend fun getGenresAllFromDatabase():List<Genre>?{
        val response= movieDao.getAllGenre()
        return response.map { it.toDomain() }
    }
    suspend fun getGenresFromDatabase(idGenre:Int):Genre?{
        val response= movieDao.getGenre(idGenre)
        return response.toDomain()
    }

    suspend fun getPlayListMovieFromApi(idMovie:Long):List<PlayListMovie>?{
        val response= api.getPlayListMovie(idMovie)
       return response?.results?.map {
           it.idMovie = idMovie
           it.toDomain()
       }
    }
    suspend fun getPlayListMovieFromDatabase(idMovie:Long):List<PlayListMovie>?{
        val response= movieDao.getPlayListMovie(idMovie)
        return response.map {it.toDomain() }
    }

    suspend fun insertMovies(moviesList:List<MoviesEntity>){
        movieDao.insertMovieAll(moviesList)
    }

    suspend fun insertGenre(moviesList:List<GenreEntity>){
        movieDao.insertGenreAll(moviesList)
    }

    suspend fun insertPlayList(moviesList:List<PlayListMovieEntity>){
        movieDao.insertPlayListMovieAll(moviesList)
    }

}