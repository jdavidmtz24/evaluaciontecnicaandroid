package com.juandavid.demogenteramovies.data.model

import com.google.gson.annotations.SerializedName

data class ListMoviesResponse(@SerializedName("average_rating") val average_rating:Double,
                              @SerializedName("backdrop_path") val backdrop_path:String,
                              //@SerializedName("comments") val comments:List<String>,
                              @SerializedName("created_by") val created_by: CreatedBy,
                              @SerializedName("description") val description:String,
                              @SerializedName("id") val id:Int,
                              @SerializedName("iso_3166_1") val iso_3166_1:String,
                              @SerializedName("iso_639_1") val iso_639_1:String,
                              @SerializedName("name") val name:String,
                              //@SerializedName("object_ids") val object_ids:List<String>,
                              @SerializedName("page") val page:Int,
                              @SerializedName("poster_path") val poster_path:String,
                              @SerializedName("public") val public:Boolean,
                              @SerializedName("results") val results:ArrayList<MoviesResult>,
                              @SerializedName("revenue") val revenue:Long,
                              @SerializedName("runtime") val runtime:Long,
                              @SerializedName("sort_by") val sort_by:String,
                              @SerializedName("total_pages") val total_pages:Int,
                              @SerializedName("total_results") val total_results:Int
)
