package com.juandavid.demogenteramovies.data.database.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.juandavid.demogenteramovies.data.model.GenreModel
import com.juandavid.demogenteramovies.domain.model.Genre
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "genre_table")
data class GenreEntity(@PrimaryKey @ColumnInfo("id") val id:Int,
                       @ColumnInfo("name") val name:String):Parcelable

fun Genre.toDatabase( ) = GenreEntity(id=id, name=name)
fun GenreModel.toDatabase( ) = GenreEntity(id=id, name=name)
