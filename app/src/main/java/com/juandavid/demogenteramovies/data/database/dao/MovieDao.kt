package com.juandavid.demogenteramovies.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.juandavid.demogenteramovies.data.database.entities.GenreEntity
import com.juandavid.demogenteramovies.data.database.entities.MoviesEntity
import com.juandavid.demogenteramovies.data.database.entities.PlayListMovieEntity

@Dao
interface MovieDao {

    @Query("SELECT * FROM movie_table where page_movie= :pageMovie order by id")
    suspend fun getMoviePage(pageMovie:Int):List<MoviesEntity>


    @Query("SELECT * FROM movie_table where id=:id")
    suspend fun getMovieId(id:Int):MoviesEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovieAll(arrayListMovie:List<MoviesEntity>)

    @Query("SELECT * FROM genre_table")
    suspend fun getAllGenre():List<GenreEntity>

    @Query("SELECT * FROM genre_table where id=:id")
    suspend fun getGenre(id:Int):GenreEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGenreAll(genreList:List<GenreEntity>)

    @Query("SELECT * FROM play_list_movie_table where id=:id")
    suspend fun getPlayListMovie(id:Long):List<PlayListMovieEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlayListMovieAll(playListMovieArray:List<PlayListMovieEntity>)
}