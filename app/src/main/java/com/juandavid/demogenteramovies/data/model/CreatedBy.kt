package com.juandavid.demogenteramovies.data.model

import com.google.gson.annotations.SerializedName

data class CreatedBy(@SerializedName("gravatar_hash") val gravatar_hash:String,
                     @SerializedName("id") val id:String,
                     @SerializedName("name") val name:String,
                     @SerializedName("username") val username:String)
