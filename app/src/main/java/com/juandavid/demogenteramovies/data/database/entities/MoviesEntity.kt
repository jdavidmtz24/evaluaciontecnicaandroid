package com.juandavid.demogenteramovies.data.database.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.juandavid.demogenteramovies.domain.model.Movie
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "movie_table")
data class MoviesEntity (@ColumnInfo("adult") val adult:Boolean,
                         @ColumnInfo("backdrop_path") val backdrop_path:String,
                         @ColumnInfo("genre_ids") val genre_ids:String,
                         @PrimaryKey @ColumnInfo("id") val id:Long,
                         @ColumnInfo("media_type") val media_type:String,
                         @ColumnInfo("original_language") val original_language:String,
                         @ColumnInfo("original_title") val original_title:String,
                         @ColumnInfo("overview") val overview:String,
                         @ColumnInfo("popularity") val popularity:Double,
                         @ColumnInfo("poster_path") val poster_path:String,
                         @ColumnInfo("release_date") val release_date:String,
                         @ColumnInfo("title") val title:String,
                         @ColumnInfo("video") val video:String,
                         @ColumnInfo("vote_average") val vote_average:String,
                         @ColumnInfo("vote_count") val vote_count:String,
                         @ColumnInfo("page_movie") val page_movie:Int): Parcelable

fun Movie.toDatabase() =
    MoviesEntity(adult = adult, backdrop_path = backdrop_path, genre_ids = genre_ids.toString(), id = id, media_type=media_type,
        original_language= original_language, original_title=original_title, overview=overview,
        popularity = popularity, poster_path=poster_path, release_date =release_date, title=title, video = video,
        vote_average = vote_average, vote_count = vote_count, page_movie = page_movie)