package com.juandavid.demogenteramovies.data.model

import com.google.gson.annotations.SerializedName
import com.juandavid.demogenteramovies.data.model.GenreModel

data class GenerMovieResponse(@SerializedName("genres") val genres:ArrayList<GenreModel>)
