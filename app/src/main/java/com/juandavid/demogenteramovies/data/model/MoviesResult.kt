package com.juandavid.demogenteramovies.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.juandavid.demogenteramovies.domain.model.Movie
import com.juandavid.demogenteramovies.utils.Constants
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MoviesResult(@SerializedName("adult") val adult:Boolean,
                        @SerializedName("backdrop_path") val backdrop_path:String,
                        @SerializedName("genre_ids") val genre_ids:ArrayList<Int>,
                        @SerializedName("id") val id:Long,
                        @SerializedName("media_type") val media_type:String,
                        @SerializedName("original_language") val original_language:String,
                        @SerializedName("original_title") val original_title:String,
                        @SerializedName("overview") val overview:String,
                        @SerializedName("popularity") val popularity:Double,
                        @SerializedName("poster_path") val poster_path:String,
                        @SerializedName("release_date") val release_date:String,
                        @SerializedName("title") val title:String,
                        @SerializedName("video") val video:String,
                        @SerializedName("vote_average") val vote_average:String,
                        @SerializedName("vote_count") val vote_count:String,
                        var page_movie:Int

) : Parcelable

fun Movie.toDomain() =
    MoviesResult(adult = adult, backdrop_path = backdrop_path, genre_ids = Constants.converStringtoArray(genre_ids), id = id, media_type=media_type,
        original_language= original_language, original_title=original_title, overview=overview,
        popularity = popularity, poster_path=poster_path, release_date =release_date, title=title, video = video,
        vote_average = vote_average, vote_count = vote_count, page_movie =page_movie)
