package com.juandavid.demogenteramovies.data.network

import com.juandavid.demogenteramovies.data.model.GenerMovieResponse
import com.juandavid.demogenteramovies.data.model.ListMoviesResponse
import com.juandavid.demogenteramovies.data.model.PlayListMovieResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesApiClient {

    @GET("4/list/{list_id}")
    suspend fun getAllMovies(
        @Path("list_id") list_id: Int,
        @Query("api_key") api_key: String
    ): Response<ListMoviesResponse>

    @GET("3/genre/movie/list")
    suspend fun getAllGenres(@Query("api_key") api_key: String): Response<GenerMovieResponse>

    @GET("3/movie/{movie_id}/videos")
    suspend fun getAllVideoMovie(
        @Path("movie_id") movie_id: Long,
        @Query("api_key") api_key: String
    ): Response<PlayListMovieResponse>

}