package com.juandavid.demogenteramovies.data.model

import com.google.gson.annotations.SerializedName

data class Video(@SerializedName("iso_639_1") val iso_639_1:String,
                 @SerializedName("iso_3166_1") val iso_3166_1:String,
                 @SerializedName("name") val name:String,
                 @SerializedName("key") val key:String,
                 @SerializedName("site") val site:String,
                 @SerializedName("size") val size:Long,
                 @SerializedName("type") val type:String,
                 @SerializedName("official") val official:String,
                 @SerializedName("published_at") val published_at:String,
                 @SerializedName("id") val id:String,
                 @SerializedName("idMovie") var idMovie:Long)
