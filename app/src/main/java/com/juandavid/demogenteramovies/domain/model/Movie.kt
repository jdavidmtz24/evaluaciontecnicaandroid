package com.juandavid.demogenteramovies.domain.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.juandavid.demogenteramovies.data.database.entities.MoviesEntity
import com.juandavid.demogenteramovies.data.model.MoviesResult

data class Movie(val adult:Boolean,
                 val backdrop_path:String,
                 val genre_ids:String,
                 val id:Long,
                 val media_type:String,
                 val original_language:String,
                 val original_title:String,
                 val overview:String,
                 val popularity:Double,
                 val poster_path:String,
                 val release_date:String,
                 val title:String,
                 val video:String,
                 val vote_average:String,
                 val vote_count:String,
                 var page_movie:Int)

fun MoviesResult.toDomain() =
    Movie(adult = adult, backdrop_path = backdrop_path, genre_ids = genre_ids.toString(), id = id, media_type=media_type,
    original_language= original_language, original_title=original_title, overview=overview,
    popularity = popularity, poster_path=poster_path, release_date =release_date, title=title, video = video,
    vote_average = vote_average, vote_count = vote_count, page_movie =page_movie)

fun MoviesEntity.toDomain() =
    Movie(adult = adult, backdrop_path = backdrop_path, genre_ids = genre_ids.toString(), id = id, media_type=media_type,
        original_language= original_language, original_title=original_title, overview=overview,
        popularity = popularity, poster_path=poster_path, release_date =release_date, title=title, video = video,
        vote_average = vote_average, vote_count = vote_count, page_movie = page_movie)