package com.juandavid.demogenteramovies.domain

import com.juandavid.demogenteramovies.data.MoviesRepository
import com.juandavid.demogenteramovies.data.database.entities.toDatabase
import com.juandavid.demogenteramovies.domain.model.Genre
import javax.inject.Inject

class GetGenrsMovieUsesCase @Inject constructor( private val repository : MoviesRepository) {

    suspend operator fun invoke():List<Genre>?{

        val getGenrsList = repository.getAllGenresFromApi()
        return if(getGenrsList != null) {
            if (getGenrsList.isNotEmpty()){
                repository.insertGenre(getGenrsList.map { it.toDatabase() })
                getGenrsList
            }else{
                repository.getGenresAllFromDatabase()
            }
        }
        else{
            repository.getGenresAllFromDatabase()
        }
    }
    suspend operator fun invoke(idGenre:Int):Genre? {
        return repository.getGenresFromDatabase(idGenre)
    }

}