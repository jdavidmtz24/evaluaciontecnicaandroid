package com.juandavid.demogenteramovies.domain.model

import com.juandavid.demogenteramovies.data.database.entities.GenreEntity
import com.juandavid.demogenteramovies.data.model.GenreModel


data class Genre(val id:Int, val name:String)

fun GenreModel.toDomain( ) = Genre(id=id, name=name)
fun GenreEntity.toDomain( ) = Genre(id=id, name=name)
