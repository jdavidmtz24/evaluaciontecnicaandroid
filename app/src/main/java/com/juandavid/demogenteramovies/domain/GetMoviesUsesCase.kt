package com.juandavid.demogenteramovies.domain

import com.juandavid.demogenteramovies.data.MoviesRepository
import com.juandavid.demogenteramovies.data.database.entities.toDatabase
import com.juandavid.demogenteramovies.data.model.ListMoviesResponse
import com.juandavid.demogenteramovies.domain.model.Movie
import javax.inject.Inject


class GetMoviesUsesCase @Inject constructor(private val repository :MoviesRepository) {


    suspend operator fun invoke(idList:Int):List<Movie>?{
        val movieList = repository.getAllMovieFromApi(idList)
        return if (movieList != null) {
            if (movieList.isNotEmpty()){
                repository.insertMovies(movieList.map {
                    it.page_movie = idList
                    it.toDatabase()
                })
                movieList
            }else{
                repository.getAllMovieFromDatabase(idList)
            }
        }
        else{
            repository.getAllMovieFromDatabase(idList)
        }
    }
}