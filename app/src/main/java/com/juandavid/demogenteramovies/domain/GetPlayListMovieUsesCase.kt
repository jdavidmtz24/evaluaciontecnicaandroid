package com.juandavid.demogenteramovies.domain

import com.juandavid.demogenteramovies.data.MoviesRepository
import com.juandavid.demogenteramovies.data.database.entities.toDatabase
import com.juandavid.demogenteramovies.data.model.GenerMovieResponse
import com.juandavid.demogenteramovies.data.model.PlayListMovieResponse
import com.juandavid.demogenteramovies.domain.model.PlayListMovie
import javax.inject.Inject

class GetPlayListMovieUsesCase @Inject constructor(private val repository : MoviesRepository){

    suspend operator fun invoke(idMovie:Long): List<PlayListMovie>?{

        val getPlayListMovie = repository.getPlayListMovieFromApi(idMovie)
        return if (getPlayListMovie != null){
            if (getPlayListMovie.isNullOrEmpty()){
               repository.insertPlayList(getPlayListMovie.map { it.toDatabase() })
                getPlayListMovie
            }
            else{
                repository.getPlayListMovieFromDatabase(idMovie)
            }
        }
        else{
            repository.getPlayListMovieFromDatabase(idMovie)
        }


    }
}