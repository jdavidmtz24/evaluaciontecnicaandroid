package com.juandavid.demogenteramovies.domain.model


import com.juandavid.demogenteramovies.data.database.entities.PlayListMovieEntity
import com.juandavid.demogenteramovies.data.model.Video

data class PlayListMovie(val iso_639_1:String,
                         val iso_3166_1:String,
                         val name:String,
                         val key:String,
                         val site:String,
                         val size:Long,
                         val type:String,
                         val official:String,
                         val published_at:String,
                         val id:String,
                         val idMovie:Long)

fun Video.toDomain() = PlayListMovie(iso_639_1 =iso_639_1, iso_3166_1 = iso_3166_1, name = name,
            key = key,site = site, size = size, type = type, official = official, published_at =published_at,
                id=id, idMovie = idMovie)
fun PlayListMovieEntity.toDomain() = PlayListMovie(iso_639_1 =iso_639_1, iso_3166_1 = iso_3166_1, name = name,
    key = key,site = site, size = size, type = type, official = official, published_at =published_at,
    id=id, idMovie = idMovie)
