package com.juandavid.demogenteramovies.ui.details_movie

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.juandavid.demogenteramovies.BuildConfig
import com.juandavid.demogenteramovies.data.model.MoviesResult
import com.juandavid.demogenteramovies.databinding.FragmentDetailMovieBinding
import com.juandavid.demogenteramovies.domain.model.PlayListMovie
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailMovieFragment : Fragment() {

    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!
    companion object {
        fun newInstance() = DetailMovieFragment()
    }
    private var movie:MoviesResult? = null
    private lateinit var viewModel: DetailMovieViewModel
    var videoUrl = "https://www.youtube.com/watch?v="
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this)[DetailMovieViewModel::class.java]
        _binding = FragmentDetailMovieBinding.inflate(inflater, container, false)
        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        movie = requireArguments().getParcelable("modelMovie")
        Log.e("Object",movie.toString())

        if (movie!= null) {
            viewModel.onCreate(movie!!.id)
        }
        viewModel.isLoading.observe(viewLifecycleOwner){
            binding.progressDetailMovie.isVisible = it
        }
        loadingVideoMovie()
        viewModel.listPlayListMovie.observe(viewLifecycleOwner){
            loadingDataMovie(movie)
            if (it != null) {
                if (it.size>=1) {
                    binding.videoMovie.isVisible=true
                    loadingVideoMovie(it)
                }
            }else{
                loadingVideoMovie()
                //binding.videoMovie.isVisible=false
            }
        }
    }
    private fun loadingVideoMovie() {

            videoUrl= "https://www.youtube.com/watch?v=kpsIu6goKGg"
            val uri: Uri = Uri.parse(videoUrl)
            binding.videoMovie.setVideoURI(uri)
            val mediaController = MediaController(context)

            // sets the anchor view
            // anchor view for the videoView

            // sets the anchor view
            // anchor view for the videoView
            mediaController.setAnchorView(binding.videoMovie)

            // sets the media player to the videoView

            // sets the media player to the videoView
            mediaController.setMediaPlayer(binding.videoMovie)

            // sets the media controller to the videoView

            // sets the media controller to the videoView
            binding.videoMovie.setMediaController(mediaController)

            // starts the video

            // starts the video
            binding.videoMovie.start()

    }
    private fun loadingVideoMovie(it: List<PlayListMovie>?) {
        if (it.isNullOrEmpty()) {
            videoUrl+= it?.get(0)?.key ?: String
            val uri: Uri = Uri.parse(videoUrl)
            binding.videoMovie.setVideoURI(uri)
            val mediaController = MediaController(context)

            // sets the anchor view
            // anchor view for the videoView

            // sets the anchor view
            // anchor view for the videoView
            mediaController.setAnchorView(binding.videoMovie)

            // sets the media player to the videoView

            // sets the media player to the videoView
            mediaController.setMediaPlayer(binding.videoMovie)

            // sets the media controller to the videoView

            // sets the media controller to the videoView
            binding.videoMovie.setMediaController(mediaController)

            // starts the video

            // starts the video
            binding.videoMovie.start()
        }
    }

    private fun loadingDataMovie(movie: MoviesResult?) {
        binding.progressDetailMovie.isVisible =true
        if (movie != null) {
            val urlImageMovie = BuildConfig.BASE_IMAGE + movie.backdrop_path
            Log.e("urlImageMovie",urlImageMovie)
            Picasso.get().load(urlImageMovie).into(binding.imageDetailMovie)
            binding.textTitleDetail.text = movie.title
            binding.textViewComents.text = "Sinopsis: "+movie.overview
            binding.textViewFechaLanzamiento.text = "Fecha de Lanzamiento: "+movie.release_date
            binding.textViewLanguage.text = "Idioma: "+movie.original_language
        }
        binding.progressDetailMovie.isVisible =false
    }

}