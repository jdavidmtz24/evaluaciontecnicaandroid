package com.juandavid.demogenteramovies.ui.peliculas

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.juandavid.demogenteramovies.R
import com.juandavid.demogenteramovies.adapter.MovieListAdapter
import com.juandavid.demogenteramovies.data.model.MoviesResult
import com.juandavid.demogenteramovies.databinding.FragmentPeliculasBinding
import com.juandavid.demogenteramovies.ui.details_movie.DetailMovieFragment
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PeliculasFragment : Fragment() {

    companion object {
        fun newInstance() = PeliculasFragment()
    }

    private  var _binding: FragmentPeliculasBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: PeliculasViewModel
    var arrayListNumber : ArrayList<String>? = null
    var navController:NavController? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PeliculasViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPeliculasBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onCreate()
        listNumber()

        viewModel.isLoading.observe(viewLifecycleOwner){
            binding.progressLoadingMovies.isVisible = it
        }
        dataRecyclerView()
    }

    private fun dataRecyclerView() {
        viewModel.listMovies.observe(viewLifecycleOwner){
            val movieAdapter = MovieListAdapter(it,MovieListAdapter.OnClickListener{
                //setupNavigation(binding.root,it)
                navigationDetail(it)
            })
            binding.recyclerPeliculas.adapter = movieAdapter
            binding.recyclerPeliculas.layoutManager = LinearLayoutManager(context)
            binding.recyclerPeliculas.setHasFixedSize(true)
        }
    }

    private fun navigationDetail(movie: MoviesResult) {
        val bundle = Bundle()
        bundle.putParcelable("modelMovie", movie)
        val fragment=DetailMovieFragment.newInstance()
        fragment.arguments = bundle
        //findNavController().navigate(R.id.action_navigation_home_movie_to_detail_movie, bundle)
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.container, fragment,"Peliculas")
            ?.commitNow()
        /*val navHostFragment = activity?.supportFragmentManager?.findFragmentById(R.id.container)
        navHostFragment?.findNavController()
            ?.navigate(R.id.action_nav_home_movie_to_detail_movie, bundle)*/
    }
    private fun setupNavigation(root: View,movie: MoviesResult) {
        val bundle = Bundle()
        bundle.putParcelable("modelMovie", movie)
        view?.let { Navigation.findNavController(it).navigate(R.id.action_nav_home_movie_to_detail_movie,bundle) }

    }
    private fun listNumber() {
       viewModel.listPage.observe(viewLifecycleOwner){
           arrayListNumber = it
           val arrayAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, it)
           binding.spinnerPageMovies.adapter = arrayAdapter
       }
    }
}