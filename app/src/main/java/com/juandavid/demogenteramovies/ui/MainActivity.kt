package com.juandavid.demogenteramovies.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.juandavid.demogenteramovies.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val peliculasIntent = Intent(this@MainActivity, PeliculasActivity::class.java)
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(peliculasIntent)
            finish()
        }, SPLASH_TIME_OUT.toLong())
    }
    companion object {
        const val SPLASH_TIME_OUT = 2000
    }
}