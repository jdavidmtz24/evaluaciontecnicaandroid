package com.juandavid.demogenteramovies.ui.details_movie

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.juandavid.demogenteramovies.data.model.GenrsProvider
import com.juandavid.demogenteramovies.domain.GetGenrsMovieUsesCase
import com.juandavid.demogenteramovies.domain.GetMoviesUsesCase
import com.juandavid.demogenteramovies.domain.GetPlayListMovieUsesCase
import com.juandavid.demogenteramovies.domain.model.Genre
import com.juandavid.demogenteramovies.domain.model.PlayListMovie
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(private val getMoviesUsesCase : GetMoviesUsesCase,
                                               private val getGenrsMovieUsesCase: GetGenrsMovieUsesCase,
                                               private val getPlayListMovieUsesCase: GetPlayListMovieUsesCase) : ViewModel() {
    val isLoading = MutableLiveData<Boolean>()
    val listPlayListMovie = MutableLiveData<List<PlayListMovie>?>()
    val providerGenrsProvider = MutableLiveData<List<Genre>?>()

    fun onCreate(idMovie: Long) {
        viewModelScope.launch {
            /*
            *
            * isLoading.postValue(true)
            val result = idPageMovie.value?.let { getMoviesUsesCase.invoke( 1) }
            if (result != null) {
                //listMovies.value = result.results
                listMovies.postValue(result.map { it.toDomain()} as ArrayList<MoviesResult>?)
            }
            val genreResult = getGenrsMovieUsesCase.invoke()
            if (genreResult != null){
                Log.e("Genero Peliculas", genreResult.toString())
            }else{
                Log.e("Genero Peliculas", "No devuelve valores")
            }
            isLoading.postValue(false)
            * */

            isLoading.postValue(true)
            var result = getPlayListMovieUsesCase.invoke(idMovie)
            if(result != null){
                listPlayListMovie.postValue(result)
            }

            isLoading.postValue(false)
        }
    }
    fun searchGenre(idGenre:Int){
        viewModelScope.launch {
            isLoading.postValue(true)
            var resultDos= getGenrsMovieUsesCase.invoke()
            if (resultDos != null) {
                providerGenrsProvider.postValue(resultDos)
            }


           isLoading.postValue(true)
        }

    }
}