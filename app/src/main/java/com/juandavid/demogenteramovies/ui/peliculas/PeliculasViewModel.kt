package com.juandavid.demogenteramovies.ui.peliculas

import android.util.Log
import android.widget.Toast
import androidx.core.util.rangeTo
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.juandavid.demogenteramovies.data.model.MoviesResult
import com.juandavid.demogenteramovies.data.model.toDomain
import com.juandavid.demogenteramovies.domain.GetGenrsMovieUsesCase
import com.juandavid.demogenteramovies.domain.GetMoviesUsesCase
import com.juandavid.demogenteramovies.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PeliculasViewModel @Inject constructor(private val getMoviesUsesCase : GetMoviesUsesCase,
                                             private val getGenrsMovieUsesCase: GetGenrsMovieUsesCase) : ViewModel() {
    private val listPageMoview = MutableLiveData<ArrayList<String>>().apply {
        value = Constants.firstFortyTwoInts()
    }
    val listPage:LiveData<ArrayList<String>> = listPageMoview
    private val _idPageMovie = MutableLiveData<Int>().apply {
        value = 1
    }
    val idPageMovie = _idPageMovie


    val listMovies = MutableLiveData<ArrayList<MoviesResult>>()

    val isLoading = MutableLiveData<Boolean>()
    fun onCreate() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = idPageMovie.value?.let { getMoviesUsesCase.invoke( 1) }
            if (result != null) {
                //listMovies.value = result.results
                listMovies.postValue(result.map { it.toDomain()} as ArrayList<MoviesResult>?)
            }
            val genreResult = getGenrsMovieUsesCase.invoke()
            if (genreResult != null){
                Log.e("Genero Peliculas", genreResult.toString())
            }else{
                Log.e("Genero Peliculas", "No devuelve valores")
            }
            isLoading.postValue(false)

        }
    }

}