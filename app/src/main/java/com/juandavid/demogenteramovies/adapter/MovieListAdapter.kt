package com.juandavid.demogenteramovies.adapter

import android.media.Image
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.juandavid.demogenteramovies.BuildConfig
import com.juandavid.demogenteramovies.R
import com.juandavid.demogenteramovies.data.model.MoviesResult
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import org.w3c.dom.Text
import java.lang.Exception

class MovieListAdapter(arrayList: ArrayList<MoviesResult>, private val onClickListener: OnClickListener): RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    private val modelMovieArrayList: ArrayList<MoviesResult>

    class ViewHolder(item: View): RecyclerView.ViewHolder(item){
        val itemImageMovie:ImageView
        val itemTitleMovie: TextView
        val itemDetailMovie: TextView
        val itemProgressMovie: ProgressBar
        init {
            itemImageMovie = item.findViewById(R.id.image_view_movie_item)
            itemTitleMovie = item.findViewById(R.id.text_view_title_movie)
            itemDetailMovie = item.findViewById(R.id.text_view_item_detail)
            itemProgressMovie = item.findViewById(R.id.progressbar_item_movie)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_movie_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return modelMovieArrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemProgressMovie.isVisible = true
        val modelMovie: MoviesResult= modelMovieArrayList[position]
        val urlImageMovie = BuildConfig.BASE_IMAGE+modelMovie.backdrop_path
        Log.e("urlImageMovie",urlImageMovie)
        Picasso.get().load(urlImageMovie).into(holder.itemImageMovie, object : Callback{
            override fun onSuccess() {
                holder.itemProgressMovie.isVisible = false
            }
            override fun onError(e: Exception?) {
                holder.itemProgressMovie.isVisible = false
            }
        })
        holder.itemTitleMovie.text = modelMovie.title
        holder.itemDetailMovie.text = modelMovie.overview
        holder.itemView.setOnClickListener {
            onClickListener.clickListener(modelMovie)
        }
    }

    init {
        this.modelMovieArrayList = arrayList
    }

    class OnClickListener(val clickListener: (modelMovie: MoviesResult) -> Unit){
        fun onClick(modelMovie: MoviesResult)= clickListener(modelMovie)
    }
}