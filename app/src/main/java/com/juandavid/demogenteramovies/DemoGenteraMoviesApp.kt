package com.juandavid.demogenteramovies

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DemoGenteraMoviesApp : Application()