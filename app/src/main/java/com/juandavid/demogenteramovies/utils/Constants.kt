package com.juandavid.demogenteramovies.utils

object Constants {
    fun firstFortyTwoInts(): ArrayList<String> = ArrayList<String>().apply {
        for (i in 1..42)
            this.add(i.toString())
    }
    fun converStringtoArray(cadena :String ): ArrayList<Int> = ArrayList<Int>().apply {
        var array= cadena.removeSurrounding("[","]").replace(" ","").split(",").map { it.toInt() }
    }
}